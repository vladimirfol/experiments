import time


class Timer:
    """
    Засекает время работы (system+user процесса)
    - с времени запуска до текущего момента, если еще не вышли из контекста
    - с времени запуска до времени выхода из контекста, если вышли из контекста

    with Timer() as t:
        func1()
        print("finish func1 in {}sec".format(t))
        func2()
    print("finish all in {}sec".format(t))
    """
    def __enter__(self):
        self.start = time.process_time()
        self.end = None
        return self

    def __exit__(self, *args):
        self.end = time.process_time()

    @property
    def t(self) -> float:
        if self.end is not None:
            return self.end - self.start
        else:
            return time.process_time() - self.start

    def __str__(self):
        return "{:.4f}".format(self.t)
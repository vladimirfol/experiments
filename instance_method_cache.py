from functools import lru_cache
import random


class C:
    def __init__(self):
        pass

    @lru_cache(maxsize=2)
    def f(self, x):
        return random.randrange(100)


c1 = C()
c2 = C()

print('c1')
print(c1.f(1))
print(c1.f(1))
print(c1.f(1))
print(c1.f(2))
print(c1.f(3))
print(c1.f(1))
print(c1.f(1))
print(c1.f(1))
print('c2')
print(c2.f(1))
print(c2.f(1))
